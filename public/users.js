var usuarios = {
  url:'https://jsonplaceholder.typicode.com/users',
  dataType: 'json',
  method:'GET'
}

$.ajax(usuarios)
  .done(printArticles)

function printArticles(postear){
  console.log(postear);
  postear.forEach(function(post){
    var elementos = '<tr>' +
    '<td>' + post.id + '</td>' +
    '<td>' + post.name + '</td>' +
    '<td>' + post.username + '</td>' +
    '<td>' + post.email + '</td>' +
    '<td>' + post.address.city + '</td>' +
    '</tr>';

    $('table').append(elementos)
  })
}
